<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wp-course' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '-(Ntl8*` 1H;W>^b4qX2Tj0EsVuswk,3[PhvMTIWMwefxBy]6,3w}#;8O,RN~W#v' );
define( 'SECURE_AUTH_KEY',  'DTaE_5<:c!d@.?U#&8sL^IP<vx~Vyyz6nEOoU<c}b9T20=L5Zbl1M(&eVtl!X{Sb' );
define( 'LOGGED_IN_KEY',    'qmTM{0>~ox*ui:u.e2n>S8}h>?max;C(Xj4kWE%U/cAg0@/$Uln%}Dz#L%T:JsWD' );
define( 'NONCE_KEY',        'G|QQ4Lk|4@.zND(%N1gHB.PGj(39cJK Eu:B#[&Vt]&h<^~0>(sBKB)v[EW,$258' );
define( 'AUTH_SALT',        't xLW*eMqzk]U$$1l(5hwR!2RAq6MoVABNS^ JI/qEP,Vv{p#&3HR!]pE-v$@oAL' );
define( 'SECURE_AUTH_SALT', 'Oql;FKXN7>3Faf.Mc*9&tbap<>:3<O o%jD_x4EhO5p~VX~o/ADt-.6i^?/-%]v}' );
define( 'LOGGED_IN_SALT',   'wGxN=:K-(R+GLv)UN7jDu2rX175mDi{Dw H|/ kNH}CxBT`xUU|whL}jcy3m@e?p' );
define( 'NONCE_SALT',       '-S`favvC M+TWH4fVJ`gB]x+U qqaJD6_kn6;;6&$3GrzhZsLM7%~o|4:lyu0&p(' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wpc_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );
define('DISALLOW_FILE_EDIT', true);
define('DISALLOW_FILE_MODS', true);

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
